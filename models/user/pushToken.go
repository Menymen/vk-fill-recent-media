package user

type PushToken struct {
	Id          int    `db:"id"`
	UserId      int    `db:"user_id"`
	TokenId     int    `db:"token_id"`
	DeviceToken string `db:"device_token"`
	Platform    string `db:"platform"`
}
