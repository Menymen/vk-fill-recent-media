package models

import (
	"VkFillRecentMediaJob/connect/database"
	"VkFillRecentMediaJob/connect/rabbitmq"
	"VkFillRecentMediaJob/helper"
	"VkFillRecentMediaJob/models/vk"
	"encoding/json"
)

const maxAttempts = 7
const PushPayloadTypeVkAccessTokenExpired = "vk_access_token_expired"
const PushPayloadDefaultSound = "default"
const PushPayloadDefaultPriority = "high"

type MainConfigStructure struct {
	Rmq    rabbitmq.Config
	Db     database.Config
	Params struct {
		VkFakeStats bool
		FcmToken    string
	}
}

type JobObject struct {
	Class     string
	AccountId int
	Attempts  int
	Priority  int
}

type RecalculateJobObject struct {
	Class       string
	AccountId   int
	IsInitPrice bool
}

type PushPayload struct {
	To       string
	Title    string
	Body     string
	Type     string
	Badge    int
	Sound    string
	Priority int
}

type WallData struct {
	Post helper.WallPost
	Wall vk.Wall
}

func (obj *JobObject) CanRetry() bool {
	return obj.Attempts < maxAttempts
}

func (obj *PushPayload) Encode() (message []byte, err error) {
	type JsonPushData struct {
		Type string `json:"type"`
	}

	type JsonNotification struct {
		Sound string `json:"sound"`
		Title string `json:"title"`
		Body  string `json:"body"`
		Badge int    `json:"badge"`
	}

	type JsonPushPayload struct {
		Notification JsonNotification `json:"notification"`
		Data         JsonPushData     `json:"data"`
		Priority     string           `json:"priority"`
		To           string           `json:"to"`
	}

	pushData := JsonPushData{Type: obj.Type}
	notification := JsonNotification{
		Sound: PushPayloadDefaultSound,
		Title: obj.Title,
		Body:  obj.Body,
		Badge: obj.Badge,
	}
	pushPayload := JsonPushPayload{
		Notification: notification,
		Data:         pushData,
		Priority:     PushPayloadDefaultPriority,
		To:           obj.To,
	}

	message, err = json.Marshal(pushPayload)
	return message, err
}
