package vk

const AccountStatusActive = "active"
const bloggerTypeId = 1
const publicTypeId = 2

type Account struct {
	Id              int    `db:"id"`
	UserId          int    `db:"user_id"`
	CategoryId      int    `db:"category_id"`
	TypeId          int    `db:"type_id"`
	VkId            int    `db:"vk_id"`
	AccessToken     string `db:"access_token"`
	MediaCount      int    `db:"media_count"`
	IsFakeStatistic bool   `db:"is_fake_statistic"`
	UpdatedMediaAt  string `db:"updated_media_at"`
	IsExpired       bool   `db:"is_expired"`
}

func (account *Account) IsGroup() bool {
	return account.TypeId == publicTypeId
}

func (account *Account) GetRequestId() (vkId int) {
	vkId = account.VkId
	if account.IsGroup() {
		vkId *= -1
	}

	return vkId
}
