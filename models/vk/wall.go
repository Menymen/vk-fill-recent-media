package vk

import (
	"time"
)

type Wall struct {
	Id                 int64  `db:"id"`
	AccountId          int    `db:"account_id"`
	VkId               int    `db:"vk_id"`
	CommentCount       int    `db:"comment_count"`
	ViewCount          int    `db:"view_count"`
	LikeCount          int    `db:"like_count"`
	AnalyzedLikeCount  int    `db:"analyzed_like_count"`
	FollowersLikeCount int    `db:"followers_like_count"`
	RepostCount        int    `db:"reposts_count"`
	PostedAt           string `db:"posted_at"`
	UpdatedAt          string `db:"updated_at"`
}

func (wall *Wall) IsNewRecord() bool {
	return wall.Id == 0
}

func (wall *Wall) UpdatedAtAsTime() (updatedAt time.Time) {
	updatedAt, _ = time.Parse("2006-01-02 15:04:05", wall.UpdatedAt)
	return updatedAt
}
