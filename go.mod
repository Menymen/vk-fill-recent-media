module VkFillRecentMediaJob

go 1.18

require (
	github.com/SevereCloud/vksdk/v2 v2.15.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/joho/godotenv v1.4.0
	github.com/rabbitmq/amqp091-go v1.5.0
)

require (
	github.com/klauspost/compress v1.15.8 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
