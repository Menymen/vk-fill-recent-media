package helper

import (
	"VkFillRecentMediaJob/models/vk"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/SevereCloud/vksdk/v2/api"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"
)

const version = "5.131"

var errorMessages = [3]string{
	"api: Too many requests per second",
	"unexpected end of JSON input",
	"invalid content-type",
}

type WallPost struct {
	Id          int         `json:"id"`
	FromId      int         `json:"from_id"`
	CopyHistory *[]WallPost `json:"copy_history"`
	Comments    struct {
		Count int `json:"count"`
	} `json:"comments"`
	Views struct {
		Count int `json:"count"`
	} `json:"views"`
	Likes struct {
		Count int `json:"count"`
	} `json:"likes"`
	Reposts struct {
		Count int `json:"count"`
	} `json:"reposts"`
	PostedAt int `json:"date"`
}

func (wallPost *WallPost) PostedAtAsString() (postedAt string) {
	dateTime := time.Unix(int64(wallPost.PostedAt), 0)
	postedAt = dateTime.Format("2006-01-02 15:04:05")
	return postedAt
}

type WallPostLikes struct {
	WallId int `json:"wallId"`
	Users  []struct {
		Uid int `json:"uid"`
	} `json:"users"`
}

type Users struct {
	UserData []struct {
		Photo string `json:"photo_50"`
		Sex   int    `json:"sex"`
		BDate string `json:"bdate"`
		City  struct {
			Id int `json:"id"`
		} `json:"city"`
		Country struct {
			Id int `json:"id"`
		} `json:"country"`
		Schools struct {
			Id int `json:"id"`
		} `json:"schools"`
		Education struct {
			University int `json:"university"`
			Faculty    int `json:"faculty"`
			Graduation int `json:"graduation"`
		} `json:"education"`
		FirstNameNom string `json:"first_name_nom"`
		LastNameNom  string `json:"last_name_nom"`
	} `json:"users"`
}

func FollowerIds(account *vk.Account, errChannel chan<- error) []int {
	total, err := requestTotalFollowers(account)
	if err != nil {
		go func() {
			errChannel <- err
		}()

		return nil
	}

	offset := 25000
	var mx sync.Mutex
	var wg sync.WaitGroup
	followerIds := make([]int, 0, total)
	for baseOffset := 0; baseOffset < total; baseOffset += offset {
		wg.Add(1)
		go func(baseOffset int) {
			defer wg.Done()
			partFollowerIds, err := getFollowerIds(account, offset, baseOffset, total)
			if err != nil {
				go func() {
					errChannel <- err
				}()

				return
			}

			mx.Lock()
			defer mx.Unlock()
			for _, id := range partFollowerIds {
				followerIds = append(followerIds, id)
			}

			return
		}(baseOffset)
	}

	wg.Wait()
	return followerIds
}

func FriendIds(account *vk.Account, errChannel chan<- error) []int {
	total, err := requestTotalFriends(account)
	if err != nil {
		go func() {
			errChannel <- err
		}()

		return nil
	}

	offset := 25000
	mx := new(sync.Mutex)
	wg := new(sync.WaitGroup)
	friendIds := make([]int, 0)
	for baseOffset := 0; baseOffset < total; baseOffset += offset {
		wg.Add(1)
		go func(baseOffset int) {
			defer wg.Done()
			partFriendIds, err := getFriendIds(account, offset, baseOffset, total)
			if err != nil {
				go func() {
					errChannel <- err
				}()
				return
			}

			mx.Lock()
			defer mx.Unlock()
			for _, id := range partFriendIds {
				friendIds = append(friendIds, id)
			}

			return
		}(baseOffset)
	}

	wg.Wait()
	return friendIds
}

func WallPosts(account *vk.Account, baseOffset int, maxPosts int) (posts []WallPost, err error) {
	offset := 500
	posts, err = requestWallPosts(account, offset, baseOffset, maxPosts)
	return posts, err
}

func WallPostsLikes(account *vk.Account, postIds []int) (wallPostLikes []WallPostLikes, err error) {
	wallPostLikes, err = requestWallPostLikes(account, postIds)
	return wallPostLikes, err
}

func WallPostCount(account *vk.Account) (total int, err error) {
	vkClient := api.NewVK(account.AccessToken)
	params := api.Params{
		"owner_id": account.GetRequestId(),
		"filter":   "owner",
		"count":    1,
		"offset":   0,
		"v":        version,
	}

	response := api.WallGetResponse{}
	for {
		response, err = vkClient.WallGet(params)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return total, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	total = response.Count
	return total, err
}

func UsersAccountInfo(account *vk.Account, userIds []int) (users *api.UsersGetResponse, err error) {
	users, err = requestUsersAccountInfo(account, userIds)
	return users, err
}

func requestTotalFollowers(account *vk.Account) (total int, err error) {
	vkClient := api.NewVK(account.AccessToken)
	if account.IsGroup() {
		params := api.Params{
			"group_id": account.VkId,
			"v":        version,
		}

		groupResponse := api.GroupsGetMembersResponse{}
		for {
			groupResponse, err = vkClient.GroupsGetMembers(params)
			if err == nil {
				break
			} else if !InArray(err.Error(), errorMessages) {
				return total, err
			} else {
				time.Sleep(100 * time.Millisecond)
			}
		}

		total = groupResponse.Count
	} else {
		params := api.Params{
			"user_id": account.VkId,
			"v":       version,
		}

		userResponse := api.UsersGetFollowersResponse{}
		for {
			userResponse, err = vkClient.UsersGetFollowers(params)
			if err == nil {
				break
			} else if !InArray(err.Error(), errorMessages) {
				return total, err
			} else {
				time.Sleep(100 * time.Millisecond)
			}
		}

		total = userResponse.Count
	}

	return total, err
}

func requestTotalFriends(account *vk.Account) (total int, err error) {
	vkClient := api.NewVK(account.AccessToken)
	if account.IsGroup() {
		err = errors.New("groups hasn't friends")
		return total, err
	}

	params := api.Params{
		"user_id": account.VkId,
		"v":       version,
	}

	response := api.FriendsGetResponse{}
	for {
		response, err = vkClient.FriendsGet(params)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return total, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	total = response.Count
	return total, err
}

func getFollowerIds(
	account *vk.Account,
	offset int,
	baseOffset int,
	totalFollowers int,
) (ids []int, err error) {
	ids, err = requestMemberOrFollowerIds(account, offset, baseOffset, totalFollowers)
	if err != nil {
		log.Println("Error whiling get follower ids", err)
		return nil, err
	}

	return ids, nil
}

func getFriendIds(
	account *vk.Account,
	offset int,
	baseOffset int,
	totalFriends int,
) (ids []int, err error) {
	ids, err = requestFriendIds(account, offset, baseOffset, totalFriends)
	if err != nil {
		log.Println("Error whiling get friend ids", err)
		return nil, err
	}

	return ids, nil
}

func requestMemberOrFollowerIds(account *vk.Account, offset int, baseOffset int, maxMembers int) (ids []int, err error) {
	if offset > 25000 {
		return nil, errors.New("offset max is 250000")
	}

	limit := 1000
	if offset < limit || offset%limit != 0 {
		return nil, errors.New("offset must be more 1000 and must be a multiple of it")
	}

	vkClient := api.NewVK(account.AccessToken)
	var code string
	if account.IsGroup() {
		code = fmt.Sprintf(`
		var members = [];
		var offset = 0;
		while (offset < %d && (offset + %d) < %d){
			members = members + "," + API.groups.getMembers({"group_id": %d, "v": "%s", "sort": "time_desc", "count": %d, "offset": offset + %d}).items;
			offset = offset + %d;
		};
		return members;`,
			offset, baseOffset, maxMembers, account.VkId, version, limit, baseOffset, limit,
		)
	} else {
		code = fmt.Sprintf(`
		var followers = [];
		var offset = 0;
		while (offset < %d && (offset + %d) < %d){
			followers = followers + "," + API.users.getFollowers({"user_id": %d, "v": "%s", "count": %d, "offset": offset + %d}).items;
			offset = offset + %d;
		};
		return followers;`,
			offset, baseOffset, maxMembers, account.VkId, version, limit, baseOffset, limit,
		)
	}

	params := api.Params{
		"code": code,
		"v":    version,
	}

	var responseInterface interface{}
	for {
		err = vkClient.ExecuteWithArgs(code, params, &responseInterface)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return nil, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	response := fmt.Sprint(responseInterface)
	stringIds := strings.Split(response, ",")
	idsLength := len(stringIds) - 1
	var id int
	for i := 1; i <= idsLength; i++ {
		id, _ = strconv.Atoi(stringIds[i])
		ids = append(ids, id)
	}

	return ids, nil
}

func requestFriendIds(account *vk.Account, offset int, baseOffset int, maxFriends int) (ids []int, err error) {
	if offset > 25000 {
		return nil, errors.New("offset max is 250000")
	}

	limit := 1000
	if offset < limit || offset%limit != 0 {
		return nil, errors.New("offset must be more 1000 and must be a multiple of it")
	}

	vkClient := api.NewVK(account.AccessToken)
	code := fmt.Sprintf(`
		var friends = [];
		var offset = 0;
		while (offset < %d && (offset + %d) < %d){
			friends = friends + "," + API.friends.get({"user_id": %d, "v": "%s", "count": %d, "offset": offset + %d}).items;
			offset = offset + %d;
		};
		return friends;`,
		offset, baseOffset, maxFriends, account.VkId, version, limit, baseOffset, limit,
	)

	params := api.Params{
		"code": code,
		"v":    version,
	}

	var responseInterface interface{}
	for {
		err = vkClient.ExecuteWithArgs(code, params, &responseInterface)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return nil, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	response := fmt.Sprint(responseInterface)
	stringIds := strings.Split(response, ",")
	idsLength := len(stringIds) - 1
	var id int
	for i := 1; i <= idsLength; i++ {
		id, _ = strconv.Atoi(stringIds[i])
		ids = append(ids, id)
	}

	return ids, nil
}

func requestWallPosts(account *vk.Account, offset int, baseOffset int, maxPosts int) (posts []WallPost, err error) {
	if offset > 500 {
		return nil, errors.New("offset max is 500")
	}

	limit := 100
	if offset < limit || offset%limit != 0 {
		return nil, errors.New("offset must be more 1000 and must be a multiple of it")
	}

	vkClient := api.NewVK(account.AccessToken)
	code := fmt.Sprintf(`
		var posts = [];
		var offset = 0;
		var subPosts;
		var i;
		var arrayLength;
		while (offset < %d && (offset + %d) < %d)
		{
			subPosts = API.wall.get({
				"owner_id": %d,
				"filter": "owner",
				"v": "%s",
				"count": %d,
				"offset": offset + %d
			}).items;
			i = 0;
			arrayLength = subPosts.length;
			while (i < arrayLength) {
				posts.push(subPosts[i]);
				i = i + 1;
			}
			offset = offset + %d;
		};
		return {"posts": posts};`,
		offset, baseOffset, maxPosts, account.GetRequestId(), version, limit, baseOffset, limit,
	)

	params := api.Params{
		"code": code,
		"v":    version,
	}

	var response struct {
		Posts []WallPost `json:"posts"`
	}

	for {
		err = vkClient.ExecuteWithArgs(code, params, &response)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return nil, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	posts = response.Posts
	return posts, nil
}

func requestWallPostLikes(account *vk.Account, postIds []int) (wallPostLikes []WallPostLikes, err error) {
	if len(postIds) > 25 {
		return nil, errors.New("can't process more 500 ids")
	}

	vkClient := api.NewVK(account.AccessToken)
	var postIdsByteJson []byte
	postIdsByteJson, err = json.Marshal(postIds)
	postIdsJson := string(postIdsByteJson)
	if err != nil {
		return nil, err
	}

	code := fmt.Sprintf(`var data = [];
		var subData;
		var ids = %s;
		var len = ids.length;
		var index = 0;
		while (index < len) {
			subData = API.wall.getLikes({
				"owner_id": %d,
				"post_id": ids[index],
				"count": 1000,
				"offset": 0,
				"v": "%s",
			}).users;

			
			data.push({
				wallId: ids[index],
				users: subData
			});
			index = index + 1;
		}

		return data;
`,
		postIdsJson, account.GetRequestId(), version,
	)

	params := api.Params{
		"code": code,
		"v":    version,
	}

	var response []WallPostLikes

	for {
		err = vkClient.ExecuteWithArgs(code, params, &response)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return nil, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	return response, nil
}

func requestUsersAccountInfo(account *vk.Account, usersIds []int) (userInfoResponse *api.UsersGetResponse, err error) {
	vkClient := api.NewVK(account.AccessToken)
	var requestIds string
	for _, id := range usersIds {
		if requestIds != "" {
			requestIds += ","
		}

		requestIds += strconv.Itoa(id)
	}

	fields := [11]string{
		"photo_50",
		"sex",
		"counters",
		"bdate",
		"city",
		"country",
		"schools",
		"education",
		"first_name_nom",
		"last_name_nom",
		"last_seen",
	}

	params := api.Params{
		"user_ids": requestIds,
		"v":        version,
		"fields":   fields,
	}

	response := api.UsersGetResponse{}
	for {
		response, err = vkClient.UsersGet(params)
		if err == nil {
			break
		} else if !InArray(err.Error(), errorMessages) {
			return nil, err
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}

	userInfoResponse = &response
	return userInfoResponse, err
}
