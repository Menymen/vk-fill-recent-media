package helper

import (
	"encoding/json"
	"reflect"
)

func InArray(val interface{}, array interface{}) bool {
	values := reflect.ValueOf(array)

	if reflect.TypeOf(array).Kind() == reflect.Slice || values.Len() > 0 {
		for i := 0; i < values.Len(); i++ {
			if reflect.DeepEqual(val, values.Index(i).Interface()) {
				return true
			}
		}
	}

	return false
}

func Serialize(obj interface{}) (message []byte, err error) {
	message, err = json.Marshal(obj)
	return message, err
}
