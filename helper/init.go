package helper

import (
	"errors"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func CheckEnv() {
	if err := godotenv.Load(); err != nil {
		log.Fatalf("No .env file found")
	}

	var env [10]bool
	_, env[0] = os.LookupEnv("RABBIT_MQ_HOST")
	_, env[1] = os.LookupEnv("RABBIT_MQ_PORT")
	_, env[2] = os.LookupEnv("RABBIT_MQ_USER")
	_, env[3] = os.LookupEnv("RABBIT_MQ_PASS")
	_, env[4] = os.LookupEnv("MYSQL_HOST")
	_, env[5] = os.LookupEnv("MYSQL_PORT")
	_, env[6] = os.LookupEnv("MYSQL_USERNAME")
	_, env[7] = os.LookupEnv("MYSQL_PASSWORD")
	_, env[8] = os.LookupEnv("MYSQL_DATABASE")
	_, env[9] = os.LookupEnv("FCM_TOKEN")

	for i := 0; i < len(env); i++ {
		if env[i] == false {
			err := errors.New("env not set")
			log.Fatalf("%s", err)
		}
	}
}
