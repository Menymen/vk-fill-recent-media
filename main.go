package main

import (
	"VkFillRecentMediaJob/connect/database"
	"VkFillRecentMediaJob/connect/rabbitmq"
	"VkFillRecentMediaJob/helper"
	"VkFillRecentMediaJob/models"
	"VkFillRecentMediaJob/models/vk"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// {"class":"common\\modules\\vk\\jobs\\FillRecentMediaJob","accountId":60,"attempts":0,"priority":254,"totalSaved":null}
var GlobalApp struct {
	Config models.MainConfigStructure
	Rmq    rabbitmq.AmqpInterface
	Db     database.DbInterface
}

func init() {
	var err error
	helper.CheckEnv()

	isFakeStats, exist := os.LookupEnv("VK_FAKE_STATS")
	GlobalApp.Config.Params.VkFakeStats = exist && isFakeStats == "1"
	GlobalApp.Config.Params.FcmToken, _ = os.LookupEnv("FCM_TOKEN")

	GlobalApp.Config.Rmq.Host, _ = os.LookupEnv("RABBIT_MQ_HOST")
	GlobalApp.Config.Rmq.Port, _ = os.LookupEnv("RABBIT_MQ_PORT")
	GlobalApp.Config.Rmq.User, _ = os.LookupEnv("RABBIT_MQ_USER")
	GlobalApp.Config.Rmq.Password, _ = os.LookupEnv("RABBIT_MQ_PASS")

	GlobalApp.Rmq, err = rabbitmq.NewAmqDial(GlobalApp.Config.Rmq)
	if err != nil {
		log.Fatal(err)
	}

	GlobalApp.Config.Db.Host, _ = os.LookupEnv("MYSQL_HOST")
	GlobalApp.Config.Db.Port, _ = os.LookupEnv("MYSQL_PORT")
	GlobalApp.Config.Db.User, _ = os.LookupEnv("MYSQL_USERNAME")
	GlobalApp.Config.Db.Password, _ = os.LookupEnv("MYSQL_PASSWORD")
	GlobalApp.Config.Db.Database, _ = os.LookupEnv("MYSQL_DATABASE")
	maxConnectionString, _ := os.LookupEnv("MYSQL_MAX_CONТECTION")
	maxConnection, err := strconv.Atoi(maxConnectionString)
	if err != nil {
		log.Fatal(err)
	}

	GlobalApp.Config.Db.MaxConnection = maxConnection
	GlobalApp.Db, err = database.NewDbApp(GlobalApp.Config.Db)
	if err != nil {
		log.Fatal(err)
	}
}

// todo Логирование
func main() {
	defer GlobalApp.Rmq.Close()
	defer GlobalApp.Db.Close()
	rmqChannel := make(chan []byte)
	go GlobalApp.Rmq.Receive(rmqChannel)
	for rmqMessage := range rmqChannel {
		go handle(rmqMessage)
	}
}

func handle(rmqMessage []byte) {
	var jobObject models.JobObject
	err := json.Unmarshal(rmqMessage, &jobObject)
	if err != nil {
		log.Printf("%s: %v", "Error whiling unserialize message", err)
		return
	}

	log.Printf("Ok, got the job, accountId: %d, priority: %d", jobObject.AccountId, jobObject.Priority)
	start := time.Now()

	vkAccount, err := GlobalApp.Db.FindAccount(jobObject.AccountId)
	err = handleError(&jobObject, err, "Error whiling find vkAccount")
	if err != nil {
		return
	}

	if vkAccount == nil {
		log.Printf("Can not find VkAccount by id: %d", jobObject.AccountId)
		return
	}

	needFirstRecalculate := vkAccount.MediaCount == 0
	var followerIds []int
	var friendIds []int
	var wgGlobal sync.WaitGroup
	wgGlobal.Add(1)
	err = nil
	errChannel := make(chan error)
	go func() {
		defer wgGlobal.Done()
		followerIds = helper.FollowerIds(vkAccount, errChannel)
	}()

	if vkAccount.IsGroup() == false {
		wgGlobal.Add(1)
		go func() {
			defer wgGlobal.Done()
			friendIds = helper.FriendIds(vkAccount, errChannel)
		}()
	}

	needExit := false
	go func() {
		for err = range errChannel {
			if err.Error() == "User authorization failed: invalid access_token (4)." {
				wgGlobal.Add(1)
				defer wgGlobal.Done()
				log.Printf("Account access token expired, try logout it, accountId: %d", vkAccount.Id)
				expireErr := expireAccount(vkAccount)
				if expireErr != nil {
					log.Printf("Error whiling expire token, accountId: %ds, error: %v", vkAccount.Id, expireErr)
				} else {
					log.Printf("Account logout successfully, accountId: %d", vkAccount.Id)
				}

				needExit = true
				break
			}
		}
	}()

	wgGlobal.Wait()
	if needExit {
		log.Printf("Job finish success, accountId: %d, priority: %d", jobObject.AccountId, jobObject.Priority)
		return
	}

	err = handleError(&jobObject, err, "Error whiling get follower or friend Ids")
	if err != nil {
		return
	}

	for _, id := range friendIds {
		followerIds = append(followerIds, id)
	}

	var totalWallPosts int
	totalWallPosts, err = helper.WallPostCount(vkAccount)
	err = handleError(&jobObject, err, "Error whiling get total wall posts")
	if err != nil {
		return
	}

	defer func() {
		if panicError := recover(); panicError != nil {
			panicErrorMsg := errors.New(fmt.Sprintf("%v", panicError))
			_ = handleError(&jobObject, panicErrorMsg, "Catch panic")
		}
	}()

	wg := new(sync.WaitGroup)
	analyzedUsers := make(map[int]bool)
	for offset := 0; offset < totalWallPosts; offset += 500 {
		wg.Add(1)
		err = handleWallPosts(wg, vkAccount, totalWallPosts, &followerIds, offset, &needFirstRecalculate, analyzedUsers)
		if err != nil && strings.Contains(err.Error(), "api: execute errors") {
			continue
		}

		err = handleError(&jobObject, err, "Error whiling handle posts")
		if err != nil {
			return
		}
	}

	wg.Wait()

	mediaCount, err := GlobalApp.Db.CountWallPosts(vkAccount)
	err = handleError(&jobObject, err, "Error whiling count wall posts")
	if err != nil {
		return
	}

	if GlobalApp.Config.Params.VkFakeStats || vkAccount.IsFakeStatistic {
		mediaCount = 50
	}

	currentDate := time.Now()
	vkAccount.MediaCount = mediaCount
	vkAccount.UpdatedMediaAt = currentDate.Format("2006-01-02 15:04:05")
	err = GlobalApp.Db.UpdateAccount(vkAccount)
	err = handleError(&jobObject, err, "Error whiling update account")
	if err != nil {
		return
	}

	_ = setRecalculateJob(vkAccount.Id, false)

	end := time.Now()
	executionTime := end.Unix() - start.Unix()
	log.Printf("Executoin time: %d, account id: %d", executionTime, vkAccount.Id)
	log.Printf("Job finish success, accountId: %d, priority: %d", jobObject.AccountId, jobObject.Priority)
}

func handleWallPosts(
	parentWg *sync.WaitGroup,
	vkAccount *vk.Account,
	totalWallPosts int,
	followersIds *[]int,
	offset int,
	needFirstRecalculate *bool,
	analyzedUsers map[int]bool,
) error {
	defer parentWg.Done()
	posts, err := helper.WallPosts(vkAccount, offset, totalWallPosts)
	if err != nil {
		return err
	}

	ownerVkId := vkAccount.GetRequestId()
	lenPosts := len(posts)
	var postIds = make([]int, 0, lenPosts)
	var wallMap = make(map[int]models.WallData)

	mappingOffset := 0
	mappingLimit := 25

	var from, to int
	mxMapping := new(sync.Mutex)
	wgMapping := new(sync.WaitGroup)
	for mappingOffset < lenPosts {
		from = mappingOffset
		to = mappingOffset + mappingLimit
		if to > lenPosts {
			to = lenPosts
		}

		slicedPosts := posts[from:to]
		for _, post := range slicedPosts {
			wgMapping.Add(1)
			go func(post helper.WallPost) {
				defer wgMapping.Done()
				if post.CopyHistory != nil || post.FromId != ownerVkId {
					return
				}

				wall, err := GlobalApp.Db.FindWall(vkAccount.Id, post.Id)
				if err != nil {
					log.Printf("Error whiling find wall, accountId: %d, error: %v", vkAccount.Id, err)
					return
				}

				if wall.IsNewRecord() {
					wall.AccountId = vkAccount.Id
					wall.VkId = post.Id
				} else {
					updatedAt := wall.UpdatedAtAsTime()
					currentDate := time.Now().Add(-12 * 24 * time.Hour)
					if updatedAt.After(currentDate) {
						return
					}
				}

				mxMapping.Lock()
				postIds = append(postIds, post.Id)
				wallData := models.WallData{
					Post: post,
					Wall: wall,
				}

				wallMap[post.Id] = wallData
				mxMapping.Unlock()
			}(post)
		}

		mappingOffset += mappingLimit
	}

	wgMapping.Wait()
	offset = 0
	total := len(postIds)
	wgPost := new(sync.WaitGroup)
	mxAnalyzedMap := new(sync.RWMutex)
	for offset < total {
		from = offset
		to = offset + 25
		if to > total {
			to = total
		}

		subPostIds := postIds[from:to]
		wallPostsLikes, err := helper.WallPostsLikes(vkAccount, subPostIds)
		if err != nil {
			log.Printf("Error whiling get wall likes, acountId: %d, error: %v", vkAccount.Id, err)
			continue
		}

		for _, wallPostLikes := range wallPostsLikes {
			wgPost.Add(1)
			wallData, _ := wallMap[wallPostLikes.WallId]
			go handleOneWallPost(
				vkAccount,
				wgPost,
				wallData,
				wallPostLikes,
				followersIds,
				totalWallPosts,
				needFirstRecalculate,
				analyzedUsers,
				mxAnalyzedMap,
			)
		}

		offset += 25
	}

	wgPost.Wait()
	return nil
}

func handleOneWallPost(
	vkAccount *vk.Account,
	wgPost *sync.WaitGroup,
	wallData models.WallData,
	likesData helper.WallPostLikes,
	followersIds *[]int,
	totalWallPosts int,
	needFirstRecalculate *bool,
	analyzedUsers map[int]bool,
	mxAnalyzedMap *sync.RWMutex,
) {
	defer wgPost.Done()
	var err error
	var followersLikeCount int
	likeUserIds := make([]int, 0, len(likesData.Users))
	for _, likeUserData := range likesData.Users {
		likeUserIds = append(likeUserIds, likeUserData.Uid)
		if helper.InArray(likeUserData.Uid, *followersIds) {
			followersLikeCount++
		}
	}

	var analyzedLikeCount int
	analyzedLikeCount, err = analyzeLikes(vkAccount, likeUserIds, analyzedUsers, mxAnalyzedMap)
	if err != nil {
		log.Printf("Error whiling analyze likes, accountId: %d, error: %v", vkAccount.Id, err)
		return
	}

	wall := wallData.Wall
	post := wallData.Post
	wall.CommentCount = post.Comments.Count
	wall.ViewCount = post.Views.Count
	wall.LikeCount = post.Likes.Count
	wall.AnalyzedLikeCount = analyzedLikeCount
	wall.FollowersLikeCount = followersLikeCount
	wall.RepostCount = post.Reposts.Count
	wall.PostedAt = post.PostedAtAsString()
	err = GlobalApp.Db.SaveWall(wall)
	if err != nil {
		log.Printf("Error save wall posts, accountId: %d, error: %v", vkAccount.Id, err)
		return
	}

	recalculateJobChan := make(chan int)
	wgRecalculateRoutine := new(sync.WaitGroup)
	wgRecalculateRoutine.Add(1)
	go func(recalculateJobChan <-chan int) {
		wgRecalculateRoutine.Done()
		for accountId := range recalculateJobChan {
			err := setRecalculateJob(accountId, true)
			if err != nil {
				log.Printf("Error set calculate statistic job: %v, object: %d", err, accountId)
			}

			vkAccount.MediaCount = totalWallPosts
			err = GlobalApp.Db.UpdateAccount(vkAccount)
		}

		return
	}(recalculateJobChan)

	wgRecalculateRoutine.Wait()
	if *needFirstRecalculate == true && wall.FollowersLikeCount > 0 && wall.AnalyzedLikeCount > 0 {
		recalculateJobChan <- vkAccount.Id
		*needFirstRecalculate = false
	}

	close(recalculateJobChan)
	return
}

func analyzeLikes(
	vkAccount *vk.Account,
	likeUserIds []int,
	analyzedUsers map[int]bool,
	mxAnalyzedMap *sync.RWMutex,
) (int, error) {
	analyzed := 0
	notFoundIds := make([]int, 0)
	for _, userId := range likeUserIds {
		mxAnalyzedMap.RLock()
		isFake, exists := analyzedUsers[userId]
		mxAnalyzedMap.RUnlock()
		if !exists {
			notFoundIds = append(notFoundIds, userId)
			continue
		}

		if !isFake {
			analyzed++
		}
	}

	limit := 1000
	offset := 0
	total := len(notFoundIds)
	wg := new(sync.WaitGroup)
	mxCount := new(sync.Mutex)
	errChannel := make(chan error)
	for offset < total {
		from := offset
		to := limit + offset
		if to > total {
			to = total
		}

		wg.Add(1)
		go func(errChannel chan<- error) {
			defer wg.Done()
			slicedUserIds := notFoundIds[from:to]
			usersData, err := helper.UsersAccountInfo(vkAccount, slicedUserIds)
			if err != nil {
				go func() {
					errChannel <- err
				}()

				return
			}

			for _, user := range *usersData {
				isFake := user.Deactivated != "" || user.Photo50 == "" || user.Photo50 == "https://vk.com/images/camera_50.png" || user.FirstNameNom == "" || user.LastNameNom == "" || (user.City.ID == 0 && user.Country.ID == 0 && user.Schools == nil && user.University == 0) || (user.Bdate == "" && user.Sex == 0)
				mxAnalyzedMap.Lock()
				analyzedUsers[user.ID] = isFake
				mxAnalyzedMap.Unlock()
				if isFake {
					continue
				}

				mxCount.Lock()
				analyzed++
				mxCount.Unlock()
			}
		}(errChannel)

		offset += limit
	}

	var analyzeErr error
	go func() {
		for analyzeErr = range errChannel {
			break
		}
	}()

	wg.Wait()
	return analyzed, analyzeErr
}

func setRecalculateJob(accountId int, isInitPrice bool) (err error) {
	jobObject := models.RecalculateJobObject{
		Class:       "common\\modules\\vk\\jobs\\CalculateAccountStatisticsJob",
		AccountId:   accountId,
		IsInitPrice: isInitPrice,
	}

	message, err := helper.Serialize(jobObject)
	if err != nil {
		return err
	}

	queueName := rabbitmq.RecalculateStatusQueue
	GlobalApp.Rmq.Publish(message, queueName)
	log.Printf("Set recalculate job, accountId: %d, is init price: %t", accountId, isInitPrice)
	return nil
}

func handleError(jobObject *models.JobObject, err error, errMsg string) error {
	if err == nil {
		return nil
	}

	log.Printf(errMsg+": %v", err)
	if jobObject.CanRetry() {
		jobObject.Attempts += 1
		var message []byte
		var resendErr error
		message, resendErr = helper.Serialize(jobObject)
		if resendErr != nil {
			log.Printf("Error whiling resend job into queue: %v, object: %v", resendErr, jobObject)
			return err
		}

		queueName := rabbitmq.FillRecentMediaQueue
		GlobalApp.Rmq.Publish(message, queueName)
	} else {
		log.Printf("Attempts ended, vkId: %d", jobObject.AccountId)
	}

	return err
}

func expireAccount(vkAccount *vk.Account) (err error) {
	vkAccount.IsExpired = true

	err = GlobalApp.Db.UpdateAccount(vkAccount)
	if err != nil {
		return err
	}

	title := "Авторизуйтесь в приложении"
	message := "Ваша сессия авторизации VK истекла, для продолжения сбора статистики и работы приложения необходимо авторизоваться"
	pushTokens, err := GlobalApp.Db.FindPushTokens(vkAccount)
	if err != nil {
		log.Printf("Error whiling fine user push tokens, userId: %d, error: %v", vkAccount.UserId, err)
	}

	for _, pushToken := range *pushTokens {
		pushPayload := &models.PushPayload{
			To:    pushToken.DeviceToken,
			Title: title,
			Body:  message,
			Type:  models.PushPayloadTypeVkAccessTokenExpired,
			Badge: 1,
		}

		body, _ := pushPayload.Encode()
		httpClient := &http.Client{}
		request, _ := http.NewRequest("post", "https://fcm.googleapis.com/fcm/send", bytes.NewBuffer(body))
		request.Header.Add("Content-Type", "application/json")
		request.Header.Add("Authorization", "key="+GlobalApp.Config.Params.FcmToken)
		response, err := httpClient.Do(request)
		if err != nil || response.StatusCode != 200 {
			log.Printf("Error shiling send push. Error: %v, response: %v", err, response)
		}
	}

	err = GlobalApp.Db.DeleteUserTokens(vkAccount)
	if err != nil {
		log.Printf("Erorr whiling delete user tokens, accountId: %d, error: %v", vkAccount.Id, err)
	}

	err = GlobalApp.Db.DeleteUserPushTokens(vkAccount)
	if err != nil {
		log.Printf("Erorr whiling delete user push tokens, accountId: %d, error: %v", vkAccount.Id, err)
	}

	return nil
}
