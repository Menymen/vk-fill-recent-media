package rabbitmq

import (
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"time"
)

const FillRecentMediaQueue = "vk_fill_recent_media"
const RecalculateStatusQueue = "vk_calculate_account_statistics"

type Config struct {
	Host     string
	Port     string
	User     string
	Password string
}

type AmqpInterface interface {
	Receive(rmqChannel chan []byte)
	Publish(message []byte, queueName string)
	Close()
}

type AmqpConnection struct {
	connection *amqp.Connection
}

func NewAmqDial(config Config) (AmqpInterface, error) {
	url := createUrl(config)
	conn, err := amqp.Dial(url)
	handleError(err, "Can't connect to AMQP")
	connectionApp := &AmqpConnection{connection: conn}
	return connectionApp, nil
}

func (obj *AmqpConnection) Close() {
	_ = obj.connection.Close()
}

func (obj *AmqpConnection) Receive(rmqChannel chan []byte) {
	amqpChannel, err := obj.connection.Channel()
	handleError(err, "Can't create AMQP channel")
	defer amqpChannel.Close()

	queueName := FillRecentMediaQueue
	args := amqp.Table{"x-max-priority": 10}
	queue, err := amqpChannel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		args,
	)
	handleError(err, "Failed to declare a queue")
	messageChannel, err := amqpChannel.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	handleError(err, "Could not register consumer")
	var forever chan struct{}
	go func() {
		for message := range messageChannel {
			rmqChannel <- message.Body
		}
	}()

	<-forever
}

func (obj *AmqpConnection) Publish(message []byte, queueName string) {
	amqpChannel, err := obj.connection.Channel()
	handleError(err, "Can't create AMQP channel")
	defer amqpChannel.Close()

	args := amqp.Table{"x-max-priority": 10}
	queue, err := amqpChannel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		args,
	)
	handleError(err, "Failed to declare a queue")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = amqpChannel.PublishWithContext(
		ctx,
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
			Priority:    9,
		})

	handleError(err, "Can't create AMQP channel")
}

func createUrl(config Config) string {
	var url string
	url = fmt.Sprintf(
		"amqp://%s:%s@%s:%s/",
		config.User, config.Password, config.Host, config.Port,
	)

	return url
}

func handleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
