package database

import (
	"VkFillRecentMediaJob/models/user"
	"VkFillRecentMediaJob/models/vk"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

type Config struct {
	Host          string
	Port          string
	User          string
	Password      string
	Database      string
	MaxConnection int
}

type DbInterface interface {
	Close()
	FindAccount(accountId int) (*vk.Account, error)
	FindWall(accountId int, vkId int) (vk.Wall, error)
	SaveWall(wall vk.Wall) error
	CountWallPosts(account *vk.Account) (int, error)
	UpdateAccount(account *vk.Account) error
	FindPushTokens(account *vk.Account) (*[]user.PushToken, error)
	DeleteUserTokens(account *vk.Account) error
	DeleteUserPushTokens(account *vk.Account) error
}

type DbConnection struct {
	db *sql.DB
}

func NewDbApp(config Config) (DbInterface, error) {
	//
	connStr := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		config.User, config.Password, config.Host, config.Port, config.Database,
	)
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(config.MaxConnection)

	err = db.Ping()
	// Проверка соединения с базой данных
	if err != nil {
		return nil, err
	}

	dbApp := &DbConnection{db: db}

	return dbApp, nil
}

func (obj *DbConnection) Close() {
	_ = obj.db.Close()
}

func (obj *DbConnection) FindAccount(accountId int) (*vk.Account, error) {
	query := `
		SELECT 
			id,
			user_id,
			category_id,
			type_id,
			IFNULL(vk_id, 0),
			IFNULL(access_token, ""),	
			media_count,
		   	is_fake_statistic,
		   	updated_media_at,
		   	is_expired
		FROM vk_account
		WHERE id = ?
			AND status = ?
			AND is_expired = ?
		LIMIT 1
`
	rows, err := obj.db.Query(query, accountId, vk.AccountStatusActive, false)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		account := new(vk.Account)
		err = rows.Scan(
			&account.Id,
			&account.UserId,
			&account.CategoryId,
			&account.TypeId,
			&account.VkId,
			&account.AccessToken,
			&account.MediaCount,
			&account.IsFakeStatistic,
			&account.UpdatedMediaAt,
			&account.IsExpired,
		)

		if err != nil {
			continue
		}

		return account, nil
	}

	return nil, nil
}

func (obj *DbConnection) FindWall(accountId int, vkId int) (vk.Wall, error) {
	wall := new(vk.Wall)
	query := `
		SELECT 
			id,
			account_id,
			vk_id,
			comment_count,
			IFNULL(view_count, 0),
			like_count,
			IFNULL(analyzed_like_count, 0),
			IFNULL(followers_like_count, 0),
			IFNULL(reposts_count, 0),
			posted_at,
			updated_at
		FROM vk_wall
		WHERE account_id = ?
			AND vk_id = ?
		LIMIT 1
`
	rows, err := obj.db.Query(query, accountId, vkId)
	if err != nil {
		return *wall, err
	}

	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(
			&wall.Id,
			&wall.AccountId,
			&wall.VkId,
			&wall.CommentCount,
			&wall.ViewCount,
			&wall.LikeCount,
			&wall.AnalyzedLikeCount,
			&wall.FollowersLikeCount,
			&wall.RepostCount,
			&wall.PostedAt,
			&wall.UpdatedAt,
		)

		if err != nil {
			continue
		}

		return *wall, nil
	}

	return *wall, nil
}

func (obj *DbConnection) SaveWall(wall vk.Wall) error {
	var query string
	var err error
	var result sql.Result
	if wall.IsNewRecord() {
		query = `
			INSERT INTO vk_wall
			(account_id, vk_id, comment_count, view_count, like_count, analyzed_like_count, followers_like_count, reposts_count, posted_at)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
`

		result, err = obj.db.Exec(
			query,
			wall.AccountId,
			wall.VkId,
			wall.CommentCount,
			wall.ViewCount,
			wall.LikeCount,
			wall.AnalyzedLikeCount,
			wall.FollowersLikeCount,
			wall.RepostCount,
			wall.PostedAt,
		)
	} else {
		query = `
			UPDATE vk_wall
				SET comment_count = ?,
					view_count = ?,
					like_count = ?,
					analyzed_like_count = ?,
					followers_like_count = ?,
					reposts_count = ?,
					posted_at = ?
				WHERE id = ?
`
		result, err = obj.db.Exec(
			query,
			wall.CommentCount,
			wall.ViewCount,
			wall.LikeCount,
			wall.AnalyzedLikeCount,
			wall.FollowersLikeCount,
			wall.RepostCount,
			wall.PostedAt,
			wall.Id,
		)
	}

	if result != nil && wall.IsNewRecord() {
		wall.Id, _ = result.LastInsertId()
	}

	return err
}

func (obj *DbConnection) CountWallPosts(account *vk.Account) (count int, err error) {
	query := `
		SELECT 
			COUNT(*) as count
		FROM vk_wall
		WHERE account_id = ?
`
	rows, err := obj.db.Query(query, account.Id)
	if err != nil {
		return 0, err
	}

	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(
			&count,
		)

		return count, err
	}

	return 0, nil
}

func (obj *DbConnection) UpdateAccount(account *vk.Account) error {
	var query string
	query = `
		UPDATE vk_account
			SET media_count = ?
				AND is_expired = ?
			WHERE id = ?
`
	_, err := obj.db.Exec(
		query,
		account.MediaCount,
		account.IsExpired,
		account.Id,
	)

	return err
}

func (obj *DbConnection) FindPushTokens(account *vk.Account) (*[]user.PushToken, error) {
	query := `
		SELECT 
			id,
			user_id,
			IFNULL(token_id, 0),
			device_token,
			platform
		FROM user_push_tokens
		WHERE user_id = ?
`
	rows, err := obj.db.Query(query, account.UserId)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	pushTokens := make([]user.PushToken, 0)
	for rows.Next() {
		pushToken := new(user.PushToken)
		err = rows.Scan(
			&pushToken.Id,
			&pushToken.UserId,
			&pushToken.TokenId,
			&pushToken.DeviceToken,
			&pushToken.Platform,
		)

		if err != nil {
			continue
		}

		pushTokens = append(pushTokens, *pushToken)
	}

	return &pushTokens, nil
}

func (obj *DbConnection) DeleteUserTokens(account *vk.Account) error {
	var query string
	query = `
		DELETE FROM user_tokens
			WHERE user_id = ?
`
	_, err := obj.db.Exec(
		query,
		account.UserId,
	)

	return err
}

func (obj *DbConnection) DeleteUserPushTokens(account *vk.Account) error {
	var query string
	query = `
		DELETE FROM user_push_tokens
			WHERE user_id = ?
`
	_, err := obj.db.Exec(
		query,
		account.UserId,
	)

	return err
}
